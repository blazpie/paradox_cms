Rails.application.routes.draw do

  # namespace :admin do
  #   get 'tickets/create'
  #   get 'tqickets/update'
  #   get 'tickets/destroy'
  # end
  namespace :admin do
    resources :events
    resources :ticket_plans do
      resources :tickets
    end
    resources :movies
    resources :shows
    get '/', to: 'static#index'
  end

end