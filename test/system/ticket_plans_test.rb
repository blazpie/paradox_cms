require "application_system_test_case"

class TicketPlansTest < ApplicationSystemTestCase
  setup do
    @ticket_plan = ticket_plans(:one)
  end

  test "visiting the index" do
    visit ticket_plans_url
    assert_selector "h1", text: "Ticket Plans"
  end

  test "creating a Ticket plan" do
    visit ticket_plans_url
    click_on "New Ticket Plan"

    fill_in "Name", with: @ticket_plan.name
    fill_in "Tickets", with: @ticket_plan.tickets_id
    click_on "Create Ticket plan"

    assert_text "Ticket plan was successfully created"
    click_on "Back"
  end

  test "updating a Ticket plan" do
    visit ticket_plans_url
    click_on "Edit", match: :first

    fill_in "Name", with: @ticket_plan.name
    fill_in "Tickets", with: @ticket_plan.tickets_id
    click_on "Update Ticket plan"

    assert_text "Ticket plan was successfully updated"
    click_on "Back"
  end

  test "destroying a Ticket plan" do
    visit ticket_plans_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ticket plan was successfully destroyed"
  end
end
