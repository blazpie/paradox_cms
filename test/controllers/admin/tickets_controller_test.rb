require 'test_helper'

class Admin::TicketsControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get admin_tickets_create_url
    assert_response :success
  end

  test "should get update" do
    get admin_tickets_update_url
    assert_response :success
  end

  test "should get destroy" do
    get admin_tickets_destroy_url
    assert_response :success
  end

end
