require 'test_helper'

class TicketPlansControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ticket_plan = ticket_plans(:one)
  end

  test "should get index" do
    get ticket_plans_url
    assert_response :success
  end

  test "should get new" do
    get new_ticket_plan_url
    assert_response :success
  end

  test "should create ticket_plan" do
    assert_difference('TicketPlan.count') do
      post ticket_plans_url, params: { ticket_plan: { name: @ticket_plan.name, tickets_id: @ticket_plan.tickets_id } }
    end

    assert_redirected_to ticket_plan_url(TicketPlan.last)
  end

  test "should show ticket_plan" do
    get ticket_plan_url(@ticket_plan)
    assert_response :success
  end

  test "should get edit" do
    get edit_ticket_plan_url(@ticket_plan)
    assert_response :success
  end

  test "should update ticket_plan" do
    patch ticket_plan_url(@ticket_plan), params: { ticket_plan: { name: @ticket_plan.name, tickets_id: @ticket_plan.tickets_id } }
    assert_redirected_to ticket_plan_url(@ticket_plan)
  end

  test "should destroy ticket_plan" do
    assert_difference('TicketPlan.count', -1) do
      delete ticket_plan_url(@ticket_plan)
    end

    assert_redirected_to ticket_plans_url
  end
end
