class AddMissingMovieFields < ActiveRecord::Migration[5.2]
  def change
    add_column :movies, :description, :string
    add_column :movies, :country, :string
    add_column :movies, :year, :integer
  end
end
