class CreateMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :movies do |t|
      t.string :title
      t.string :directed_by
      t.string :screenplay_by
      t.string :staring
      t.integer :duration
      t.string :production
      t.string :genre

      t.timestamps
    end
  end
end
