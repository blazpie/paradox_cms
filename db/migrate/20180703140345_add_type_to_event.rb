class AddTypeToEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :is_cycle, :boolean, default: false
  end
end
