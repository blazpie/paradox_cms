class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :title
      t.references :movies, foreign_key: true

      t.timestamps
    end
  end
end
