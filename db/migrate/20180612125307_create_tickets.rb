class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :ticket_plans do |t|
      t.string :name

      t.timestamps
    end

    create_table :tickets do |t|
      t.string :name
      t.decimal :price
      t.references :ticket_plan, foreign_key: true

      t.timestamps
    end

  end
end
