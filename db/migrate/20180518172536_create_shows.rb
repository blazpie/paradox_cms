class CreateShows < ActiveRecord::Migration[5.2]
  def change
    create_table :shows do |t|
      t.references :movie, foreign_key: true
      t.timestamp :start_time

      t.timestamps
    end
  end
end
