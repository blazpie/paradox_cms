class ChangeEventsTable < ActiveRecord::Migration[5.2]
  def change
    remove_reference :events, :movies, foreign_key: true
    add_column :events, :description, :string
    add_reference :shows, :event, foreign_key: true
    add_reference :shows, :ticket_plan, foreign_key: true
  end
end
