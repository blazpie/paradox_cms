# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_05_153423) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "events", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "description"
    t.string "image"
    t.boolean "is_cycle", default: false
  end

  create_table "movies", force: :cascade do |t|
    t.string "title"
    t.string "directed_by"
    t.string "screenplay_by"
    t.string "staring"
    t.integer "duration"
    t.string "production"
    t.string "genre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
    t.string "description"
    t.string "country"
    t.integer "year"
  end

  create_table "shows", force: :cascade do |t|
    t.bigint "movie_id"
    t.datetime "start_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "event_id"
    t.bigint "ticket_plan_id"
    t.index ["event_id"], name: "index_shows_on_event_id"
    t.index ["movie_id"], name: "index_shows_on_movie_id"
    t.index ["ticket_plan_id"], name: "index_shows_on_ticket_plan_id"
  end

  create_table "ticket_plans", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tickets", force: :cascade do |t|
    t.string "name"
    t.decimal "price"
    t.bigint "ticket_plan_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ticket_plan_id"], name: "index_tickets_on_ticket_plan_id"
  end

  add_foreign_key "shows", "events"
  add_foreign_key "shows", "movies"
  add_foreign_key "shows", "ticket_plans"
  add_foreign_key "tickets", "ticket_plans"
end
