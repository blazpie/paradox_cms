# README

Content Managment Service to be used in local cinema website. WIP.

* Ruby version: 2.5.1

* Setting up

  clone repository code, go to project folder and:
  
  ```bundle install``` to istall dependencies
  ```rails db:create``` to create databases
  ```rails db:schema:load``` to load current db schema

* Running locally
  
  ```rails s``` to start server
  
