module Admin

  class MoviesController < Admin::ApplicationController

    def index
      @id = params[:id]
      @focus_shows = params[:shows]
      if params[:search]
        @movies = Movie.where("LOWER(title) LIKE :key_word OR
        LOWER(directed_by) LIKE :key_word OR
        LOWER(screenplay_by) LIKE :key_word OR
        LOWER(staring) LIKE :key_word", {:key_word => "%#{params[:search].downcase}%"})
        .order(updated_at: :desc)
      else
        @movies = Movie.all.order(updated_at: :desc)
      end
    end

    def update
      @movie = Movie.find(params[:id])
      @movie.update(movie_params)
      if @movie.save
        flash[:success] = "Pomyślnie zmieniono"
        redirect_to :action => 'index', :id => @movie.id
      else
        flash.now[:error] = "Błąd podczas edycji filmu"
        render 'layouts/_flashTrigger'
      end
    end

    def create
      @movie = Movie.new(movie_params)
      if @movie.save
        flash[:success] = "Pomyślnie dodano"
        redirect_to :action => 'index', :id => @movie.id
      else
        flash.now[:error] = "Błąd podczas dodawania filmu"
        render 'layouts/_flashTrigger'
      end
    end

    def destroy
      @movie = Movie.find(params[:id])
      if @movie.destroy
        flash[:success] = "Pomyślnie usunięto"
        id = nil
      else
        flash[:error] = "Błąd podczas usuwania filmu"
        id = @movie.id
      end
      redirect_to :action => 'index', :id => id
    end

    private
    def movie_params
      params.require(:movie).permit(:title, :directed_by, :screenplay_by,
                                    :staring, :duration, :description, :country, :year,  :image, :remove_image)
    end

  end
end
