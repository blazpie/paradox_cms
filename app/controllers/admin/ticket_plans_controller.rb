module Admin

  class TicketPlansController < Admin::ApplicationController
    before_action :set_ticket_plan, only: [:update, :destroy]

    def index
      @ticket_plans = TicketPlan.all
    end

    def create
      @ticket_plan = TicketPlan.new(ticket_plan_params)
      if @ticket_plan.save
        flash[:success] = 'Pomyślnie dodano'
        redirect_to :action => 'index', :id => @ticket_plan.id
      else
        flash.now[:error] = 'Błąd podczas dodawania'
        render 'layouts/_flashTrigger'
      end
    end

    def update
      if @ticket_plan.update(ticket_plan_params)
        flash[:success] = 'Pomyślnie zmieniono'
        redirect_to :action => 'index', :id => @ticket_plan.id
      else
        flash.now[:error] = 'Błąd podczas edycji'
        render 'layouts/_flashTrigger'
      end
    end

    def destroy
      if @ticket_plan.destroy
        flash[:success] = 'Pomyślnie usunięto'
      else
        flash.now[:error] = 'Błąd podczas usuwania'
      end
      redirect_to :action => 'index', :id => @ticket_plan.id
    end


    private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket_plan
      @ticket_plan = TicketPlan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_plan_params
      params.require(:ticket_plan).permit(:name)
    end
  end
end
