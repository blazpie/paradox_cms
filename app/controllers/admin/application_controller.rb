module Admin
  class ApplicationController < ActionController::Base

    helper_method :flash_class
    layout "application"
    http_basic_authenticate_with name: "paradox", password: "piotrburczyn"
    def flash_class(level)
      case level
        when 'notice' then "alert alert-info"
        when 'success' then "alert alert-success text-center"
        when 'error' then "alert alert-danger"
      end
    end

  end
end
