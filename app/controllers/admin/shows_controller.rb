module Admin

  class ShowsController < Admin::ApplicationController
    include Admin::ShowsHelper

    def index
      @shows = Show.all
      @sorted_shows = get_sorted_future_shows(@shows)
    end

    def create
      @show = Show.new(show_params)
      if @show.save
        flash.now[:success] = 'Pomyślnie dodano projekcję'
        redirect_to :action => 'index', :id => @show.id
      else
        flash.now[:error] = 'Błąd podczas dodawania projekcji'
        render 'layouts/_flashTrigger'
      end
    end

    def update
      # @movie = Movie.find(params[:movie_id])
      @show = Show.find(params[:id])
      puts @show.inspect
      @show.update(show_params)
      if @show.save
        @movie.update_attribute(:updated_at, Time.now)
        flash.now[:success] = 'Pomyślnie zmieniono projekcję'
        redirect_to :action => 'index', :id => @show.id
      else
        flash.now[:error] = 'Błąd podczas edycji projekcji'
        render 'layouts/_flashTrigger'
      end
    end

    def destroy
      # @movie = Movie.find(params[:movie_id])
      @show = Show.find(params[:id])
      if @show.destroy
        flash.now[:success] = 'Pomyślnie usunięto projekcję'
      else
        flash.now[:error] = 'Błąd podczas usuwania projekcji'
      end
      redirect_to :action => 'index', :id => @show.id
    end

    private
    def show_params
      params.require(:show).permit(:start_time, :event_id, :ticket_plan_id, :movie_id)
    end

  end

end
