module Admin
  class EventsController < Admin::ApplicationController

    def index
      @id = params[:id]
      if params[:search]
        @events = Event.where("LOWER(title) LIKE :key_word OR
        LOWER(description) LIKE :key_word", {:key_word => "%#{params[:search].downcase}%"})
        .order(updated_at: :desc)
      else
        @events = Event.all.order(updated_at: :desc)
      end
      @regular_events, @cycle_events = [], []
      @events.each do |event|
        @cycle_events << event if event.is_cycle
        @regular_events << event if !event.is_cycle
      end
    end

    def update
      @event = Event.find(params[:id])
      @event.update(event_params)
      if @event.save
        flash[:success] = "Pomyślnie zmieniono"
        redirect_to :action => 'index', :id => @event.id
      else
        flash.now[:error] = "Błąd podczas edycji wydarzenia"
        render 'layouts/_flashTrigger'
      end
    end

    def create
      @event = Event.new(event_params)
      if @event.save
        flash[:success] = "Pomyślnie dodano"
        redirect_to :action => 'index', :id => @event.id
      else
        flash.now[:error] = "Błąd podczas dodawania wydarzenia"
        render 'layouts/_flashTrigger'
      end
    end

    def destroy
      @event = Event.find(params[:id])
      if @event.destroy
        flash[:success] = "Pomyślnie usunięto"
        id = nil
      else
        flash[:error] = "Błąd podczas usuwania wydarzenia"
        id = @event.id
      end
      redirect_to :action => 'index', :id => id
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:title, :description, :is_cycle, :image, :remove_image)
    end
  end
end
