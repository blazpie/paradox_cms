class Admin::TicketsController < Admin::ApplicationController

  def create
    @ticket_plan = TicketPlan.find(params[:ticket_plan_id])
    @ticket = @ticket_plan.tickets.create(ticket_params)
    if @ticket.save
      @ticket = nil
      flash.now[:success] = 'Pomyślnie dodano'
      redirect_to :controller => 'ticket_plans', :action => 'index', :id => @ticket_plan.id
    else
      flash.now[:error] = 'Błąd podczas dodawania biletu'
      render 'layouts/_flashTrigger'
    end
  end

  def update
    @ticket_plan = TicketPlan.find(params[:ticket_plan_id])
    @ticket = Ticket.find(params[:id])
    if @ticket.save
      flash.now[:success] = 'Pomyślnie zmieniono'
      redirect_to :controller => 'ticket_plans', :action => 'index', :id => @ticket_plan.id
    else
      flash.now[:error] = 'Błąd podczas dodawania biletu'
      render 'layouts/_flashTrigger'
    end
  end

  def destroy
    @ticket_plan = TicketPlan.find(params[:ticket_plan_id])
    @ticket = Ticket.find(params[:id])
    if @ticket.destroy
      flash.now[:success] = 'Pomyślnie usunięto'
    else
      flash.now[:error] = 'Błąd podczas usuwania biletu'
    end
    redirect_to :controller => 'ticket_plans', :action => 'index', :id => @ticket_plan.id
  end

  private
  def ticket_params
    params.require(:ticket).permit(:name, :price)
  end
end
