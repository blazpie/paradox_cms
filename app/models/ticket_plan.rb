class TicketPlan < ApplicationRecord

  has_many :tickets, dependent: :destroy

  validates :name, presence: true,
                    length: { in: 1..20}
end
