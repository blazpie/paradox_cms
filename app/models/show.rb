class Show < ApplicationRecord

  validates :start_time, presence: true
  validate :parent_exist
  validate :start_time_cannot_be_past

  def parent_exist
    unless movie_id || event_id
      errors.add(:movie_id, "Nie wybrano filmu lub wydarzenia do którego przypisanć projekcję")
    end
  end
  end

  def start_time_cannot_be_past
    unless start_time && start_time.future? do
      errors.add(:start_time, "must be ?future date")
    end
  end

  scope :before_next_friday, -> {
    where("start_time BETWEEN '#{DateTime.now.beginning_of_day}' AND '#{nearest_friday + 7}'")
  }

  private
  def nearest_friday
    day = Date.today
    until day.next_day.friday?
      day = day.next_day
    end
    day.next_day.to_datetime.beginning_of_day
  end

end
