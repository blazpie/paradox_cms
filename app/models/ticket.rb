class Ticket < ApplicationRecord
  
  belongs_to :ticket_plan

  validates :name, presence: true,
                    length: { in: 1..20}
end
