class Event < ApplicationRecord

  validates :title, presence: true,
                    length: { in: 1..200}

  mount_uploader :image, PosterUploader

  
end
