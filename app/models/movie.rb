class Movie < ApplicationRecord

  validates :title, presence: true,
                    length: { in: 1..200}
  validates :duration, numericality: { only_integer: true,
                                       greater_than_or_equal: 0 }

  mount_uploader :image, PosterUploader

  scope :on_screen, -> { where(:shows.before_next_friday) }



end
