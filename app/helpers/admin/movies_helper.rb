module Admin::MoviesHelper

  def get_movie_shows(event)
    Show.where(:movie_id => event.id).order(start_time: :desc)
  end

end
