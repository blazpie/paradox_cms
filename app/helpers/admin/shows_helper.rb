module Admin::ShowsHelper

  def get_show_event(show)
    if show && show.event_id
      Event.find show.event_id
    end
  end

  def get_show_movie(show)
    if show && show.movie_id
      Movie.find show.movie_id
    end
  end

  def get_show_ticket_plan(show)
    if show && show.ticket_plan_id
      TicketPlan.find show.ticket_plan_id
    end
  end

  def get_show_parent(show)
    if show && show.movie_id
      Movie.find show.movie_id
    elsif show && show.event_id
      Event.find show.event_id
    end
  end

  def get_sorted_future_shows(shows)
    shows.select { |show| show.start_time.future? }
         .sort { |a, b| a.start_time - b.start_time }
  end
end
