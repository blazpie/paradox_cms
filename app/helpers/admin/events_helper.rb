module Admin::EventsHelper

  def get_event_shows(event)
    Show.where(:event_id => event.id).order(start_time: :desc)
  end

end
