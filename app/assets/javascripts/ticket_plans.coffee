# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$(document).ready ->
  $(document).on "hide.bs.collapse", $(".accordion-card"), (event) ->
    if $(event.target).hasClass("ticketPlanContent")
      $("##{event.target.parentElement.id} .modalTrigger").css({'display': 'none'})
  $(document).on "show.bs.collapse", $(".accordion-card"), (event) ->
    if $(event.target).hasClass("ticketPlanContent")
      $("##{event.target.parentElement.id} .modalTrigger").css({'display': 'inline-block'})
