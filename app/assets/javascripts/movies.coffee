# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  $(document).on "hide.bs.modal", $(".modal"), (event) ->
    $("##{event.target.id} form").each (_index, elem) -> elem.reset()
    if $(event.target).hasClass("img-inside")
      originalSrc = $("##{event.target.id} img").data("original")
      $("##{event.target.id} img").attr("src", originalSrc)

      if (originalSrc.slice(0, 15) == '/assets/noimage')
        $("##{event.target.id} .remover").attr("disabled", "disabled")
      else
        $("##{event.target.id} .remover").removeAttr("disabled")


  $(document).on "hide.bs.collapse", $(".accordion-card"), (event) ->
    if $(event.target).hasClass("movieContent")
      $("##{event.target.id} .proj-card").collapse 'hide'
  $(document).on "show.bs.collapse", $(".accordion-card"), (event) ->
    if $(event.target).hasClass("content")
      newLocation = location.href.split("?")[0] + "?id=#{event.target.id.split("-")[1]}"
      history.pushState("", document.title, newLocation)
      $('html, body').animate({ scrollTop: ($("##{event.target.id.replace("Content", "Header")}").offset().top - $(".navbar").outerHeight()) }, 'fast');
    if $(event.target).hasClass("proj-card")
      newLocation = location.href.split("&shows=")[0] + "&shows=true"
      history.pushState("", document.title, newLocation)
      $('html, body').animate({ scrollTop: ($(event.target).parent().offset().top - $(".navbar").outerHeight()) }, 'fast');

  $('.datetimepicker').datetimepicker({minDate: moment(), locale: 'pl', stepping: 5})

@showImagePreview = (input, movieId) ->

  movieId = movieId || ''
  fileloader = new FileReader()

  fileloader.onload = (event) ->
    $("##{movieId}_imgPreview").attr("src", event.target.result)

  if input.files and input.files[0]
    fileloader.readAsDataURL(input.files[0])
    $("##{movieId}_remove_image").val(false)
    $("##{movieId}_imageRemover").removeAttr('disabled')


@removeImg = (movieId) ->
  movieId = movieId || ''
  $("##{movieId}_imgPreview").attr("src", '/assets/noimage/poster_noimage.png')
  $("##{movieId}_remove_image").val(true)
  $("##{movieId}_form").find($("input[type=file]")).val("")
  $("##{movieId}_imageRemover").attr('disabled', 'disabled')
